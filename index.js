const hasha = require('hasha')
const fs = require('fs')
const path = require('path')

module.exports = function(options) {
    options = options || {}
    options.root = options.root || '.'
    options.len = options.len || 20

    /**
     * Insert cache bursting string
     *
     * @param {string} file
     * @returns {string}
     */
    function insertHash(file) {
        const fullPath = path.join(options.root, file)
        console.log(fullPath)
        if (!fs.existsSync(fullPath)) {
            return file
        }
        const buffer = fs.readFileSync(fullPath)

        const parts = file.split('.')
        const newFile = [
            ...parts.slice(0, -2),
            hasha(buffer).slice(0, options.len),
            ...parts.slice(-2)
        ].join('.')

        fs.renameSync(fullPath, path.join(options.root, newFile));

        return newFile
    }

    /**
     * Convert js and css sources
     *
     * @param {*} tree
     * @returns {*}
     */
    function posthtmlCacheBusting(tree) {
        tree.match([
            { tag: 'link', attrs: { rel: 'stylesheet'} },
            { tag: 'script' }
        ], function(node) {
            switch (node.tag) {
                case 'link':
                    if(node.attrs.href.indexOf('http') === 0 ) {
                        return node
                    }
                    node.attrs.href = insertHash(node.attrs.href)
                    break
                case 'script':
                    if(node.attrs.src.indexOf('http') === 0 ) {
                        return node
                    }
                    node.attrs.src = insertHash(node.attrs.src)
                    break
            }
            return node
        });

        return tree
    }

    return posthtmlCacheBusting
}