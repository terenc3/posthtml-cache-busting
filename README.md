# PostHTML Cache Busting Plugin
> Replace links to sources with hashed ones

## Usage
Use a nginx rule to remove the hash from the request

```nginx configuration
rewrite ^(.+)\.(?:[\w]{20})(?:\.min)*\.(css|js)$ $1.$2 last;
```

## Examples
```html
<link href="missing-poster.scss" rel="stylesheet" crossorigin="anonymous" />
<script src="missing-poster.ts" crossorigin="anonymous"></script>
```

```html
<link href="missing-poster.adfbzwiugfbwzuertbwe.min.css" rel="stylesheet" crossorigin="anonymous" />
<script src="missing-poster.adfbzwiugfbwzuertbwe.min.js" crossorigin="anonymous"></script>
```